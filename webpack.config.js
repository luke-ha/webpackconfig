const paths = require('./path')
const path = require('path')
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin');
const Dotenv = require('dotenv-webpack');
module.exports = {
  entry: './src/index.js',
  output: {
    path: path.join(__dirname, 'public'),
    filename: 'bundle.js'
  },
  watch: true,
  devServer: {
    port: 8080,
    compress: true,
    stats: 'minimal',
    inline: true,
    contentBase: path.join(__dirname, 'public')
  },
  mode: 'development',
  devtool: 'cheap-module-source-map',
  module: {
    rules: [
      // {
      //   test: /\.js$/,
      //   exclude: /node_modules/,
      //   loader: 'babel-loader'
      // }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
        inject: true,
        minify: true,
        template: 'public/index.html',
        filename: "./index.html"
    }),
    new Dotenv(),
  ],
  // Some libraries import Node modules but don't use them in the browser.
  // Tell Webpack to provide empty mocks for them so importing them works.
  node: {
    module: "empty",
    dgram: "empty",
    dns: "mock",
    fs: "empty",
    http2: "empty",
    net: "empty",
    tls: "empty",
    child_process: "empty"
  },  
}